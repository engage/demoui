define(['lib','text!app/sidebar.tpl','app/menu'], function(LIB,tpl,m) {
	SidebarView = Backbone.View.extend({
		el: $(".main-sidebar"),
		template: _.template(tpl),
		initialize: function() {
			this.render();
			this.menus =  new MenuView({el:$('.sidebar-menu')});
		},
		render: function() {
			console.log("init sidebar!");
            this.$el.html(this.template({}));
      	}
	});	
});